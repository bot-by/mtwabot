package uk.bot_by.mtwabot;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import feign.FeignException;
import feign.Request;
import feign.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junitpioneer.jupiter.ClearEnvironmentVariable;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockserver.client.MockServerClient;
import org.mockserver.junit.jupiter.MockServerExtension;
import org.mockserver.junit.jupiter.MockServerSettings;
import org.slf4j.LoggerFactory;
import uk.bot_by.w3w.Language;
import uk.bot_by.w3w.SquaredAddress;
import uk.bot_by.w3w.What3Words;
import uk.bot_by.w3w.What3WordsException;
import uk.bot_by.w3w.Words;
import uk.bot_by.w3w.WordsRequest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.hamcrest.text.MatchesPattern.matchesPattern;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.verify.VerificationTimes.once;

@ExtendWith({MockitoExtension.class, MockServerExtension.class})
@MockServerSettings(ports = 9876)
@Tag("slow")
public class What3WordsServiceTest {

	private static final Language POLISH = Language.builder().code("pl").name("Polish").nativeName("Polski").build();
	private static final Language RUSSIAN = Language.builder().code("ru").name("Russian").nativeName("Русский").build();

	@Mock
	private Appender<ILoggingEvent> appender;
	@Captor
	private ArgumentCaptor<LoggingEvent> captorLoggingEvent;
	@Captor
	private ArgumentCaptor<WordsRequest> captorWordsRequest;
	@Spy
	private TelegramHandler lambdaHandler;
	@Mock
	private What3Words service;
	@Mock
	private SquaredAddress squaredAddress;

	@BeforeEach
	void setUp(MockServerClient mockServerClient) {
		mockServerClient.reset();
		mockServerClient.when(request("/v3/available-languages")
				                      .withMethod("GET")
				                      .withHeader("Accept", "application/json")
				                      .withHeader("X-Api-Key", "qwerty_key"))
		                .respond(response()
				                         .withBody("{\"languages\":[{\"nativeName\":\"العربية\",\"code\":\"ar\",\"name\":\"Arabic\"},{\"nativeName\":\"Ελληνικά\",\"code\":\"el\",\"name\":\"Greek\"},{\"nativeName\":\"Türkçe\",\"code\":\"tr\",\"name\":\"Turkish\"}]}"));

		((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).addAppender(appender);
		((Logger) LoggerFactory.getLogger(getClass().getPackageName())).setLevel(Level.TRACE);
	}

	@DisplayName("Available languages")
	@Test
	public void availableLanguages(MockServerClient mockServerClient) {
		// when
		Map<String, Language> languageMap = lambdaHandler.getAvailableLanguages();

		// then
		mockServerClient.verify(request("/v3/available-languages"));

		verify(appender, atLeastOnce()).doAppend(captorLoggingEvent.capture());

		List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Available languages",
				() -> assertThat(languageMap.keySet(), containsInAnyOrder("ar", "el", "tr")),
				() -> assertEquals("Available languages: Arabic,Greek,Turkish", loggingEvents.get(2).getFormattedMessage()));
	}

	@DisplayName("Available languages are cached")
	@Test
	public void availableLanguagesAreCached(MockServerClient mockServerClient) {
		// given
		lambdaHandler.getAvailableLanguages();

		// when
		Map<String, Language> languageMap = lambdaHandler.getAvailableLanguages();

		// then
		mockServerClient.verify(request("/v3/available-languages"), once());

		verify(appender, atLeastOnce()).doAppend(captorLoggingEvent.capture());

		List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Available languages",
				() -> assertThat(languageMap.keySet(), containsInAnyOrder("ar", "el", "tr")),
				() -> assertEquals("Available languages: Arabic,Greek,Turkish", loggingEvents.get(2).getFormattedMessage()));
	}

	@DisplayName("Logging is off")
	@Test
	@ClearEnvironmentVariable(key = "WHAT3WORDS_API_LOG_LEVEL")
	public void loggingIsOff() {
		// given
		((Logger) LoggerFactory.getLogger(getClass().getPackageName())).setLevel(Level.OFF);

		// when
		lambdaHandler.getAvailableLanguages();

		// then
		verify(appender, never()).doAppend(captorLoggingEvent.capture());
	}

	@DisplayName("Initialize and get copy of the what3words service")
	@Test
	public void getWhat3WordsService() {
		// when
		What3Words copyA = lambdaHandler.getWhat3WordsService();
		What3Words copyB = lambdaHandler.getWhat3WordsService();

		// then
		assertAll("What3Words service",
				() -> assertNotNull(copyA, "copy A"),
				() -> assertNotNull(copyB, "copy B"),
				() -> assertSame(copyA, copyB, "Copies are the same"));
	}

	@DisplayName("API key is missed")
	@Test
	@ClearEnvironmentVariable(key = "WHAT3WORDS_API_KEY")
	public void keyIsMissed() {
		// when
		Exception exception = assertThrows(NullPointerException.class, () -> lambdaHandler.getWhat3WordsService());

		// then
		assertEquals("what3words API key is missed", exception.getMessage(), "Exception message");
	}

	@DisplayName("API URL is missed")
	@Test
	@ClearEnvironmentVariable(key = "WHAT3WORDS_API_LOCATOR")
	public void locatorIsMissed() {
		// when
		Exception exception = assertThrows(NullPointerException.class, () -> lambdaHandler.getWhat3WordsService());

		// then
		assertEquals("what3words API URL is missed", exception.getMessage(), "Exception message");
	}

	@DisplayName("Logging of conversation with What3Words API")
	@Test
	public void loggingOfConversation() {
		// when
		lambdaHandler.getAvailableLanguages();

		//then
		verify(appender, atLeastOnce()).doAppend(captorLoggingEvent.capture());

		List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to log request and response",
				() -> assertThat("Log size", loggingEvents, hasSize(3)),
				() -> assertEquals("[What3Words#availableLanguages] ---> GET http://localhost:9876/v3/available-languages HTTP/1.1",
						loggingEvents.get(0).getFormattedMessage(), "Request"),
				() -> assertThat("Response", loggingEvents.get(1).getFormattedMessage(),
						startsWith("[What3Words#availableLanguages] <--- HTTP/1.1 200 OK (")));
	}

	@DisplayName("Empty inline query")
	@ParameterizedTest
	@CsvFileSource(files = "src/test/resources/empty-inline-query/arguments.csv", numLinesToSkip = 1)
	public void emptyInlineQuery(String languageCode, String expectedDescription, String expectedTitle, String expectedText) throws IOException {
		// given
		JSONObject emptyInlineQuery = readJSON("src/test/resources/empty-inline-query/inline-query.json");

		if (nonNull(languageCode)) {
			emptyInlineQuery.getJSONObject("from").put("language_code", languageCode);
		}

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processInlineQuery(emptyInlineQuery);

		// then
		verify(lambdaHandler, never()).getAvailableLanguages();
		verify(lambdaHandler, never()).getWhat3WordsService();

		JSONObject response = new JSONObject(responseEvent.getBody());

		assertAll("Response",
				() -> assertEquals(200, responseEvent.getStatusCode(), "Status Code"),
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "application/json")),
				() -> assertEquals(123, response.getInt("inline_query_id"), "Inline Query ID"),
				() -> assertEquals("answerInlineQuery", response.getString("method"), "Method"),
				() -> assertEquals(0, response.getInt("cache_time"), "Cache"),
				() -> assertEquals(1, response.getJSONArray("results").length(), "Result count"),
				() -> assertEquals("empty", response.getJSONArray("results").getJSONObject(0).getString("id"), "ID"),
				() -> assertEquals("article", response.getJSONArray("results").getJSONObject(0).getString("type"), "Type"),
				() -> assertEquals("example.com/icon/info.png",
						response.getJSONArray("results").getJSONObject(0).getString("thumb_url"), "Icon"),
				() -> assertEquals(expectedDescription,
						response.getJSONArray("results").getJSONObject(0).getString("description"), "Description"),
				() -> assertEquals(expectedTitle,
						response.getJSONArray("results").getJSONObject(0).getString("title"), "Title"),
				() -> assertEquals(expectedText,
						response.getJSONArray("results").getJSONObject(0).getJSONObject("input_message_content").getString("message_text"),
						"Text"));
	}

	@DisplayName("Error on inline query")
	@ParameterizedTest
	@CsvFileSource(files = "src/test/resources/server-error-inline-query/arguments.csv", numLinesToSkip = 1)
	public void serverErrorInlineQuery(String languageCode, String expectedDescription, String expectedTitle, String expectedText) throws IOException {
		// given
		JSONObject errorInlineQuery = readJSON("src/test/resources/server-error-inline-query/inline-query.json");

		if (nonNull(languageCode)) {
			errorInlineQuery.getJSONObject("from").put("language_code", languageCode);
		}

		Map<String, Language> languageMap = new HashMap<>();
		Response serviceResponse;

		languageMap.put(POLISH.getCode(), POLISH);
		languageMap.put(RUSSIAN.getCode(), RUSSIAN);
		serviceResponse = Response.builder()
		                          .request(Request.create(Request.HttpMethod.GET, "/api", Collections.emptyMap(), null, UTF_8, null))
		                          .status(500)
		                          .reason("Test Error Response")
		                          .headers(Collections.emptyMap())
		                          .body((Response.Body) null)
		                          .build();

		doReturn(languageMap).when(lambdaHandler).getAvailableLanguages();
		doReturn(service).when(lambdaHandler).getWhat3WordsService();
		doThrow(FeignException.errorStatus("method", serviceResponse)).when(service).convertToAddress(isA(WordsRequest.class));

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processInlineQuery(errorInlineQuery);

		// then
		verify(lambdaHandler, atLeastOnce()).getAvailableLanguages();
		verify(lambdaHandler).getWhat3WordsService();

		JSONObject response = new JSONObject(responseEvent.getBody());

		assertAll("Response",
				() -> assertEquals(200, responseEvent.getStatusCode(), "Status Code"),
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "application/json")),
				() -> assertEquals(987, response.getInt("inline_query_id"), "Inline Query ID"),
				() -> assertEquals("answerInlineQuery", response.getString("method"), "Method"),
				() -> assertEquals(0, response.getInt("cache_time"), "Cache"),
				() -> assertEquals(1, response.getJSONArray("results").length(), "Result count"));

		JSONObject result = response.getJSONArray("results").getJSONObject(0);

		assertAll("Result",
				() -> assertEquals("error", result.getString("id"), "ID"),
				() -> assertEquals("article", result.getString("type"), "Type"),
				() -> assertEquals("example.com/icon/error.png", result.getString("thumb_url"), "Icon"),
				() -> assertEquals(expectedDescription, result.getString("description"), "Description"),
				() -> assertEquals(expectedTitle, result.getString("title"), "Title"),
				() -> assertEquals(expectedText, result.getJSONObject("input_message_content").getString("message_text"), "Text"));
	}

	@DisplayName("Inline query with location")
	@ParameterizedTest
	@CsvFileSource(files = "src/test/resources/inline-query_location/arguments.csv", numLinesToSkip = 1)
	public void inlineQueryWithLocation(String languageCode, String expectedLanguageCode, String expectedWords) throws IOException {
		// given
		JSONObject emptyInlineQuery = readJSON("src/test/resources/inline-query_location/inline-query.json");

		if (nonNull(languageCode)) {
			emptyInlineQuery.getJSONObject("from").put("language_code", languageCode);
		}

		Map<String, Language> languageMap = new HashMap<>();

		languageMap.put(POLISH.getCode(), POLISH);
		languageMap.put(RUSSIAN.getCode(), RUSSIAN);

		doReturn(languageMap).when(lambdaHandler).getAvailableLanguages();
		doReturn(service).when(lambdaHandler).getWhat3WordsService();
		doAnswer(invocation -> {
			when(squaredAddress.getNearestPlace()).thenReturn("Larkhill, Salisbury, Wiltshire");

			WordsRequest wordsRequest = invocation.getArgument(0, WordsRequest.class);

			if (isNull(wordsRequest.getLanguage())) {
				doReturn(Words.builder().words("goodbyes.lots.dorm").build()).when(squaredAddress).getWords();
			} else {
				switch (wordsRequest.getLanguage().getCode()) {
					case "pl":
						doReturn(Words.builder().words("dobytek.belka.żarcik").build()).when(squaredAddress).getWords();
						break;
					case "ru":
						doReturn(Words.builder().words("всеведущий.прибор.доблесть").build()).when(squaredAddress).getWords();
						break;
					default:
						fail("Unexpected branch");
						break;
				}
			}

			return squaredAddress;
		}).when(service).convertToAddress(isA(WordsRequest.class));

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processInlineQuery(emptyInlineQuery);

		// then
		verify(lambdaHandler, atLeastOnce()).getAvailableLanguages();
		verify(lambdaHandler).getWhat3WordsService();
		verify(service).convertToAddress(captorWordsRequest.capture());

		if (isNull(expectedLanguageCode)) {
			assertNull(captorWordsRequest.getValue().getLanguage(), "Request language code");
		} else {
			assertEquals(expectedLanguageCode, captorWordsRequest.getValue().getLanguage().getCode(), "Request language code");
		}

		JSONObject response = new JSONObject(responseEvent.getBody());

		assertAll("Response",
				() -> assertEquals(200, responseEvent.getStatusCode(), "Status Code"),
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "application/json")),
				() -> assertEquals(321, response.getInt("inline_query_id"), "Inline Query ID"),
				() -> assertEquals("answerInlineQuery", response.getString("method"), "Method"),
				() -> assertEquals(0, response.getInt("cache_time"), "Cache"),
				() -> assertEquals(1, response.getJSONArray("results").length(), "Result count"));

		JSONObject result = response.getJSONArray("results").getJSONObject(0);

		assertAll("Result",
				() -> assertThat("ID", result.getString("id"), matchesPattern("#(-)?\\d+")),
				() -> assertEquals("article", result.getString("type"), "Type"),
				() -> assertEquals("example.com/icon/chat.png", result.getString("thumb_url"), "Icon"),
				() -> assertEquals("Larkhill, Salisbury, Wiltshire", result.getString("description"), "Description"),
				() -> assertEquals(expectedWords, result.get("title").toString(), "Title"),
				() -> assertEquals("///" + expectedWords, result.getJSONObject("input_message_content").getString("message_text"), "Text"));
	}

	@DisplayName("Ignore a message that is sent via another bot")
	@Test
	public void viaBot() throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/via-bot/message.json");

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, never()).getAvailableLanguages();
		verify(lambdaHandler, never()).getWhat3WordsService();
		verify(service, never()).convertToAddress(isA(WordsRequest.class));

		assertAll("Message via another bot",
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "text/plain")),
				() -> assertEquals("OK", responseEvent.getBody(), "Plain text body"));
	}

	@DisplayName("Message without text")
	@Test
	public void audio() throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/audio/message.json");

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, never()).getAvailableLanguages();
		verify(lambdaHandler, never()).getWhat3WordsService();
		verify(service, never()).convertToAddress(isA(WordsRequest.class));

		assertAll("Non-text message",
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "text/plain")),
				() -> assertEquals("OK", responseEvent.getBody(), "Plain text body"));
	}

	@DisplayName("Empty text")
	@Test
	public void emptyText() throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/empty-text/message.json");

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, never()).getAvailableLanguages();
		verify(lambdaHandler, never()).getWhat3WordsService();
		verify(service, never()).convertToAddress(isA(WordsRequest.class));

		assertAll("Empty text",
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "text/plain")),
				() -> assertEquals("OK", responseEvent.getBody(), "Plain text body"));
	}

	@DisplayName("Text without entities")
	@Test
	public void textWithoutEntities() throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/text-without-entities/message.json");

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, never()).getAvailableLanguages();
		verify(lambdaHandler, never()).getWhat3WordsService();
		verify(service, never()).convertToAddress(isA(WordsRequest.class));

		assertAll("Plain text",
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "text/plain")),
				() -> assertEquals("OK", responseEvent.getBody(), "Plain text body"));
	}

	@DisplayName("Message without commands")
	@Test
	public void notCommand() throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/not-command/message.json");

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, never()).getAvailableLanguages();
		verify(lambdaHandler, never()).getWhat3WordsService();
		verify(service, never()).convertToAddress(isA(WordsRequest.class));

		assertAll("Non-command text",
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "text/plain")),
				() -> assertEquals("OK", responseEvent.getBody(), "Plain text body"));
	}

	@DisplayName("Unknown command")
	@Test
	public void unknownCommand() throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/unknown-command/message.json");

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, never()).getAvailableLanguages();
		verify(lambdaHandler, never()).getWhat3WordsService();
		verify(service, never()).convertToAddress(isA(WordsRequest.class));

		assertAll("Unknown command",
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "text/plain")),
				() -> assertEquals("OK", responseEvent.getBody(), "Plain text body"));
	}

	@DisplayName("Command is not first")
	@Test
	public void messageNotStartsWithCommand() throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/message-not-starts-with-command/message.json");

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, never()).getAvailableLanguages();
		verify(lambdaHandler, never()).getWhat3WordsService();
		verify(service, never()).convertToAddress(isA(WordsRequest.class));

		assertAll("Command is not first",
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "text/plain")),
				() -> assertEquals("OK", responseEvent.getBody(), "Plain text body"));
	}

	@DisplayName("Multiple commands")
	@Test
	public void multipleCommands() throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/multiple-commands/message.json");

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, never()).getAvailableLanguages();
		verify(lambdaHandler, never()).getWhat3WordsService();
		verify(service, never()).convertToAddress(isA(WordsRequest.class));

		assertAll("Multiple commands",
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "text/plain")),
				() -> assertEquals("OK", responseEvent.getBody(), "Plain text body"));
	}

	@DisplayName("Start command in a private chat")
	@ParameterizedTest
	@CsvFileSource(files = "src/test/resources/start_private/arguments.csv", numLinesToSkip = 1)
	public void startCommandInPrivateChat(String languageCode, String expectedText, String expectedLabel) throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/start_private/message.json");

		if (nonNull(languageCode)) {
			message.getJSONObject("from").put("language_code", languageCode);
		}

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, never()).getAvailableLanguages();
		verify(lambdaHandler, never()).getWhat3WordsService();
		verify(service, never()).convertToAddress(isA(WordsRequest.class));

		JSONObject response = new JSONObject(responseEvent.getBody());

		assertAll("Response",
				() -> assertEquals(200, responseEvent.getStatusCode(), "Status Code"),
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "application/json")),
				() -> assertEquals(123456789, response.getInt("chat_id"), "Chat ID"),
				() -> assertEquals("sendMessage", response.getString("method"), "Method"),
				() -> assertEquals(expectedText, response.getString("text"), "text"),
				() -> assertTrue(response.has("reply_markup"), "Markup"),
				() -> assertTrue(response.getJSONObject("reply_markup").getBoolean("one_time_keyboard"), "One time keyboard"),
				() -> assertTrue(response.getJSONObject("reply_markup").getBoolean("resize_keyboard"), "Resize keyboard"),
				() -> assertEquals(1, response.getJSONObject("reply_markup").getJSONArray("keyboard").length(), "Keyboard count"),
				() -> assertEquals(1, response.getJSONObject("reply_markup").getJSONArray("keyboard").getJSONArray(0).length(), "Keyboard count"));

		JSONObject locationButton = response.getJSONObject("reply_markup").getJSONArray("keyboard").getJSONArray(0).getJSONObject(0);

		assertAll("Result",
				() -> assertTrue(locationButton.getBoolean("request_location"), "Request location"),
				() -> assertEquals(expectedLabel, locationButton.getString("text"), "Label"));
	}

	@DisplayName("Start command in a group chat")
	@ParameterizedTest
	@CsvFileSource(files = "src/test/resources/start_group/arguments.csv", numLinesToSkip = 1)
	public void startCommandInGroupChat(String languageCode, String expectedText) throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/start_group/message.json");

		if (nonNull(languageCode)) {
			message.getJSONObject("from").put("language_code", languageCode);
		}

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, never()).getAvailableLanguages();
		verify(lambdaHandler, never()).getWhat3WordsService();
		verify(service, never()).convertToAddress(isA(WordsRequest.class));

		JSONObject response = new JSONObject(responseEvent.getBody());

		assertAll("Response",
				() -> assertEquals(200, responseEvent.getStatusCode(), "Status Code"),
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "application/json")),
				() -> assertEquals(123456789, response.getInt("chat_id"), "Chat ID"),
				() -> assertEquals("sendMessage", response.getString("method"), "Method"),
				() -> assertEquals(expectedText, response.getString("text"), "text"),
				() -> assertFalse(response.has("reply_markup"), "Markup"));
	}

	@DisplayName("Help command")
	@ParameterizedTest
	@CsvFileSource(files = "src/test/resources/help/arguments.csv", numLinesToSkip = 1)
	public void helpCommand(String languageCode, String expectedText) throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/help/message.json");

		if (nonNull(languageCode)) {
			message.getJSONObject("from").put("language_code", languageCode);
		}

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, never()).getAvailableLanguages();
		verify(lambdaHandler, never()).getWhat3WordsService();
		verify(service, never()).convertToAddress(isA(WordsRequest.class));

		JSONObject response = new JSONObject(responseEvent.getBody());

		assertAll("Response",
				() -> assertEquals(200, responseEvent.getStatusCode(), "Status Code"),
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "application/json")),
				() -> assertEquals(123456789, response.getInt("chat_id"), "Chat ID"),
				() -> assertEquals("sendMessage", response.getString("method"), "Method"),
				() -> assertEquals(expectedText, response.getString("text"), "text"),
				() -> assertFalse(response.has("reply_markup"), "Markup"));
	}

	private JSONObject readJSON(String jsonFilepath) throws IOException {
		return new JSONObject(Files.readString(Path.of(jsonFilepath), UTF_8));
	}

	@DisplayName("Message with location but location is not found")
	@Test
	public void locationIsNotFound() throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/location-is-not-found/message.json");

		doReturn(Collections.emptyMap()).when(lambdaHandler).getAvailableLanguages();
		doReturn(service).when(lambdaHandler).getWhat3WordsService();
		doThrow(new What3WordsException(401, "InvalidKey", "Authentication failed; invalid API key")).when(service)
		                                                                                             .convertToAddress(isA(WordsRequest.class));

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, atLeastOnce()).getAvailableLanguages();
		verify(lambdaHandler).getWhat3WordsService();
		verify(service).convertToAddress(isA(WordsRequest.class));

		assertAll("Multiple commands",
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "text/plain")),
				() -> assertEquals("OK", responseEvent.getBody(), "Plain text body"));
	}

	@DisplayName("Message with location")
	@ParameterizedTest
	@CsvFileSource(files = "src/test/resources/location/arguments.csv", numLinesToSkip = 1)
	public void location(String languageCode, String expectedLanguageCode, String expectedText) throws IOException {
		// given
		JSONObject message = readJSON("src/test/resources/location/message.json");

		if (nonNull(languageCode)) {
			message.getJSONObject("from").put("language_code", languageCode);
		}

		Map<String, Language> languageMap = new HashMap<>();

		languageMap.put(POLISH.getCode(), POLISH);
		languageMap.put(RUSSIAN.getCode(), RUSSIAN);

		doReturn(languageMap).when(lambdaHandler).getAvailableLanguages();
		doReturn(service).when(lambdaHandler).getWhat3WordsService();
		doAnswer(invocation -> {
			WordsRequest wordsRequest = invocation.getArgument(0, WordsRequest.class);

			if (isNull(wordsRequest.getLanguage())) {
				doReturn(Words.builder().words("goodbyes.lots.dorm").build()).when(squaredAddress).getWords();
			} else {
				switch (wordsRequest.getLanguage().getCode()) {
					case "pl":
						doReturn(Words.builder().words("dobytek.belka.żarcik").build()).when(squaredAddress).getWords();
						break;
					case "ru":
						doReturn(Words.builder().words("всеведущий.прибор.доблесть").build()).when(squaredAddress).getWords();
						break;
					default:
						fail("Unexpected branch");
						break;
				}
			}

			return squaredAddress;
		}).when(service).convertToAddress(isA(WordsRequest.class));

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.processMessage(message);

		// then
		verify(lambdaHandler, atLeastOnce()).getAvailableLanguages();
		verify(lambdaHandler).getWhat3WordsService();
		verify(service).convertToAddress(captorWordsRequest.capture());

		if (isNull(expectedLanguageCode)) {
			assertNull(captorWordsRequest.getValue().getLanguage(), "Request language code");
		} else {
			assertEquals(expectedLanguageCode, captorWordsRequest.getValue().getLanguage().getCode(), "Request language code");
		}

		JSONObject response = new JSONObject(responseEvent.getBody());

		assertAll("Response",
				() -> assertEquals(200, responseEvent.getStatusCode(), "Status Code"),
				() -> assertThat("Content Type", responseEvent.getHeaders(), hasEntry("Content-Type", "application/json")),
				() -> assertEquals(123456789, response.getInt("chat_id"), "Chat ID"),
				() -> assertEquals("sendMessage", response.getString("method"), "Method"),
				() -> assertEquals("///" + expectedText, response.getString("text"), "Text"));
	}

}
