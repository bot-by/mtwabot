package uk.bot_by.mtwabot;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

import static java.util.Objects.isNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Tag("fast")
class TelegramHandlerTest {

	@Mock
	private Appender<ILoggingEvent> appender;
	@Captor
	private ArgumentCaptor<LoggingEvent> captorLoggingEvent;
	@Mock
	private Context context;
	@Spy
	private TelegramHandler lambdaHandler;
	@Mock
	private APIGatewayProxyResponseEvent responseEvent;

	private APIGatewayProxyRequestEvent requestEvent;

	@BeforeEach
	public void setUp() {
		requestEvent = new APIGatewayProxyRequestEvent();
		requestEvent.setHttpMethod("POST");
		requestEvent.setHeaders(Collections.singletonMap("x-forwarded-for", "1.2.3.4"));

		((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).addAppender(appender);
	}

	@DisplayName("Handle a wrong request, write IP address to info or warning log.")
	@ParameterizedTest(name = "body <{argumentsWithNames}>")
	@CsvSource({
			",Empty request from 1.2.3.4",
			"'',Empty request from 1.2.3.4",
			"' ',Empty request from 1.2.3.4",
			"name1=value1&name2=value2,Wrong request from 1.2.3.4: A JSONObject text must begin with '{' at 1 [character 2 line 1]",
	})
	public void handleWrongRequest(String body, String expectedMessage) {
		// given
		Level expectedLevel = (isNull(body) || body.isBlank()) ? Level.INFO : Level.WARN;

		requestEvent.setBody(body);

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.handleRequest(requestEvent, context);

		// then
		verify(lambdaHandler, never()).processInlineQuery(isA(JSONObject.class));
		verify(lambdaHandler, never()).processMessage(isA(JSONObject.class));
		verify(appender, atLeastOnce()).doAppend(captorLoggingEvent.capture());

		List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log an info message",
				() -> assertThat("Message", loggingEvents.get(0).getFormattedMessage(), startsWith(expectedMessage)),
				() -> assertEquals(expectedLevel, loggingEvents.get(0).getLevel(), "Level"));
		assertAll("Process Telegram request and return response",
				() -> assertNotNull(responseEvent, "Response event is not null"),
				() -> assertEquals("OK", responseEvent.getBody(), "Response body"),
				() -> assertEquals(200, responseEvent.getStatusCode(), "Response status"),
				() -> assertThat("Content type", responseEvent.getHeaders(), hasEntry("Content-Type", "text/plain")));
	}

	@DisplayName("Process an inline query")
	@Test
	public void processInlineQuery() {
		// given
		requestEvent.setBody("{ \"inline_query\": { \"query\": \"qwerty\" }}");
		when(responseEvent.getBody()).thenReturn("test inline response");
		doReturn(responseEvent).when(lambdaHandler).processInlineQuery(isA(JSONObject.class));

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.handleRequest(requestEvent, context);

		// then
		verify(lambdaHandler).processInlineQuery(isA(JSONObject.class));
		verify(lambdaHandler, never()).processMessage(isA(JSONObject.class));
		verify(appender, atLeastOnce()).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log a trace message",
				() -> assertEquals("Request body: {\"inline_query\":{\"query\":\"qwerty\"}}",
						loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.TRACE, loggingEvents.get(0).getLevel()));

		assertAll("Process Telegram request and return response",
				() -> assertNotNull(responseEvent, "Response event is not null"),
				() -> assertEquals("test inline response", responseEvent.getBody(), "Response body"));
	}

	@DisplayName("Process a message")
	@Test
	public void processMessage() {
		// given
		requestEvent.setBody("{ \"message\": { \"text\": \"test text\" }}");
		when(responseEvent.getBody()).thenReturn("test response");
		doReturn(responseEvent).when(lambdaHandler).processMessage(isA(JSONObject.class));

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.handleRequest(requestEvent, context);

		// then
		verify(lambdaHandler, never()).processInlineQuery(isA(JSONObject.class));
		verify(lambdaHandler).processMessage(isA(JSONObject.class));
		verify(appender, atLeastOnce()).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log a trace message",
				() -> assertEquals("Request body: {\"message\":{\"text\":\"test text\"}}",
						loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.TRACE, loggingEvents.get(0).getLevel()));

		assertAll("Process Telegram request and return response",
				() -> assertNotNull(responseEvent, "Response event is not null"),
				() -> assertEquals("test response", responseEvent.getBody(), "Response body"));
	}

	@DisplayName("Unprocessed update")
	@Test
	public void unprocessedUpdate() {
		// given
		requestEvent.setBody("{ \"chosen_inline_result\": { query: qwerty }}");

		// when
		APIGatewayProxyResponseEvent responseEvent = lambdaHandler.handleRequest(requestEvent, context);

		// then
		verify(lambdaHandler, never()).processInlineQuery(isA(JSONObject.class));
		verify(lambdaHandler, never()).processMessage(isA(JSONObject.class));
		verify(appender, atLeast(2)).doAppend(captorLoggingEvent.capture());

		final List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();

		assertAll("Write to the log a trace message",
				() -> assertEquals("Request body: {\"chosen_inline_result\":{\"query\":\"qwerty\"}}",
						loggingEvents.get(0).getFormattedMessage()),
				() -> assertEquals(Level.TRACE, loggingEvents.get(0).getLevel()));
		assertAll("Write to the log an info \"unprocessed\" message",
				() -> assertEquals("Unprocessed update: [chosen_inline_result]",
						loggingEvents.get(1).getFormattedMessage()),
				() -> assertEquals(Level.INFO, loggingEvents.get(1).getLevel()));

		assertAll("Process Telegram request and return response",
				() -> assertNotNull(responseEvent, "Response event is not null"),
				() -> assertEquals("OK", responseEvent.getBody(), "Response body"),
				() -> assertEquals(200, responseEvent.getStatusCode(), "Response status"),
				() -> assertThat("Content type", responseEvent.getHeaders(), hasEntry("Content-Type", "text/plain")));
	}

}
