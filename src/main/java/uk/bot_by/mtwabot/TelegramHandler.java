package uk.bot_by.mtwabot;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import feign.Feign;
import feign.FeignException;
import feign.Logger.Level;
import feign.http2client.Http2Client;
import feign.slf4j.Slf4jLogger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.VisibleForTesting;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import uk.bot_by.w3w.KeyInterceptor;
import uk.bot_by.w3w.Language;
import uk.bot_by.w3w.SquaredAddress;
import uk.bot_by.w3w.What3Words;
import uk.bot_by.w3w.What3WordsDecoder;
import uk.bot_by.w3w.What3WordsErrorDecoder;
import uk.bot_by.w3w.What3WordsException;
import uk.bot_by.w3w.WordsRequest;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static uk.bot_by.mtwabot.LambdaUtils.getResponseEvent;
import static uk.bot_by.mtwabot.LambdaUtils.responseOK;
import static uk.bot_by.mtwabot.TelegramUtils.makeAddressAnswerInlineQuery;
import static uk.bot_by.mtwabot.TelegramUtils.makeAddressMessage;
import static uk.bot_by.mtwabot.TelegramUtils.makeEmptyAnswerInlineQuery;
import static uk.bot_by.mtwabot.TelegramUtils.makeErrorAnswerInlineQuery;
import static uk.bot_by.mtwabot.TelegramUtils.makeLocationRequest;
import static uk.bot_by.mtwabot.TelegramUtils.makeMessage;

public class TelegramHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

	private static final String COMMA = ",";
	private static final String DEFAULT_LANGUAGE = "en";
	// what3words API
	private static final String WHAT_3_WORDS_API_KEY = "WHAT3WORDS_API_KEY";
	private static final String WHAT_3_WORDS_API_LOCATOR = "WHAT3WORDS_API_LOCATOR";
	private static final String WHAT_3_WORDS_API_LOG_LEVEL = "WHAT3WORDS_API_LOG_LEVEL";
	// Logger
	private static final String AVAILABLE_LANGUAGES = "Available languages: {}";
	private static final String AWS_REQUEST_ID = "AWSRequestId";
	private static final String EMPTY_REQUEST = "Empty request from {}";
	private static final String FORWARDED_FOR = "x-forwarded-for";
	private static final Logger LOGGER = LoggerFactory.getLogger(TelegramHandler.class);
	private static final String REQUEST_BODY = "Request body: {}";
	private static final String UNPROCESSED_UPDATE = "Unprocessed update: {}";
	private static final String WRONG_REQUEST = "Wrong request from {}: {}\n{}";
	// Messages
	private static final String ALTERNATIVE_LANGUAGE = "alternative-language";
	private static final String COULD_NOT_CONVERT_MESSAGE = "could-not-convert.message";
	private static final String COULD_NOT_CONVERT_TITLE = "could-not-convert.title";
	private static final String EMPTY_RESULT_HINT = "empty-result.hint";
	private static final String EMPTY_RESULT_MESSAGE = "empty-result.message";
	private static final String EMPTY_RESULT_TITLE = "empty-result.title";
	private static final String HELP_TEXT = "help";
	private static final String MESSAGES_BUNDLE_NAME = "messages";
	private static final String MY_LOCATION_TEXT = "my-location";
	private static final String WELCOME_TEXT = "welcome";
	// Telegram, update
	private static final String BOT_COMMAND = "bot_command";
	private static final String CHAT = "chat";
	private static final String FROM = "from";
	private static final String ENTITIES = "entities";
	private static final Collection<String> GROUP_CHATS = Arrays.asList("group", "supergroup", "channel");
	private static final String ID = "id";
	private static final String INLINE_QUERY = "inline_query";
	private static final String LANGUAGE_CODE = "language_code";
	private static final String LATITUDE = "latitude";
	private static final String LOCATION = "location";
	private static final String LONGITUDE = "longitude";
	private static final String MESSAGE = "message";
	private static final String OFFSET = "offset";
	private static final String TEXT = "text";
	private static final String TYPE = "type";
	private static final String VIA_BOT = "via_bot";
	// Command
	private static final Function<JSONArray, Stream<JSONObject>> ENTITY_STREAM =
			(entities) -> StreamSupport.stream(entities.spliterator(), true)
			                           .map(object -> (JSONObject) object);
	private static final Predicate<Stream<JSONObject>> HAS_COMMANDS =
			(entityStream) -> entityStream
					                  .map(entity -> entity.get(TYPE))
					                  .anyMatch(BOT_COMMAND::equals);
	private static final Predicate<Stream<JSONObject>> ONLY_ONE_COMMAND =
			(entityStream) -> entityStream
					                  .map(entity -> entity.get(TYPE))
					                  .filter(BOT_COMMAND::equals)
					                  .count() == 1;
	private static final Pattern PATTERN_COMMAND_HELP = Pattern.compile("/help([?@])?.*");
	private static final Pattern PATTERN_COMMAND_START = Pattern.compile("/start([?@])?.*");
	private static final Predicate<Stream<JSONObject>> START_WITH_COMMAND =
			(entityStream) -> entityStream
					                  .filter(entity -> 0 == (entity.getInt(OFFSET)))
					                  .anyMatch(entity -> BOT_COMMAND.equals(entity.getString(TYPE)));
	// Process text
	private static final String REGEX_SPACE = "\\s+";

	private Map<String, Language> availableLanguages;
	private What3Words threeWordAddressService;

	/**
	 * The handler checks that a request is not empty and contains JSON body.
	 * <p>
	 * If body has <em>inline query</em> or <em>message</em> it calls proper method. In other case just returns <em>OK<em> response.
	 *
	 * @param requestEvent bot input
	 * @param context      context object
	 * @return Telegram method call (JSON) or <em>OK</em> (plain text).
	 */
	@Override
	public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent requestEvent, Context context) {
		Optional<APIGatewayProxyResponseEvent> responseEvent = empty();

		MDC.put(AWS_REQUEST_ID, context.getAwsRequestId());

		if (null == requestEvent.getBody() || requestEvent.getBody().isBlank()) {
			LOGGER.info(EMPTY_REQUEST, requestEvent.getHeaders().get(FORWARDED_FOR));
		} else {
			try {
				JSONObject update = new JSONObject(requestEvent.getBody());

				LOGGER.trace(REQUEST_BODY, update);
				if (update.has(INLINE_QUERY)) {
					responseEvent = Optional.of(processInlineQuery(update.getJSONObject(INLINE_QUERY)));
				} else if (update.has(MESSAGE)) {
					responseEvent = Optional.of(processMessage(update.getJSONObject(MESSAGE)));
				} else {
					LOGGER.info(UNPROCESSED_UPDATE, update.keySet());
				}
			} catch (JSONException exception) {
				LOGGER.warn(WRONG_REQUEST, requestEvent.getHeaders().get(FORWARDED_FOR), exception.getMessage(),
						requestEvent.getBody());
			}
		}

		return responseEvent.orElseGet(LambdaUtils::responseOK);
	}

	@VisibleForTesting
	@NotNull
	Map<String, Language> getAvailableLanguages() {
		if (isNull(availableLanguages)) {
			synchronized (this) {
				availableLanguages = getWhat3WordsService().availableLanguages()
				                                           .stream()
				                                           .collect(Collectors.toMap(Language::getCode, language -> language));
				if (LOGGER.isTraceEnabled()) {
					StringJoiner joiner = new StringJoiner(COMMA);
					availableLanguages.values().forEach(language -> joiner.add(language.getName()));
					LOGGER.trace(AVAILABLE_LANGUAGES, joiner);
				}
			}
		}

		return availableLanguages;
	}

	@VisibleForTesting
	@NotNull
	What3Words getWhat3WordsService() {
		if (isNull(threeWordAddressService)) {
			synchronized (this) {
				String key = System.getenv(WHAT_3_WORDS_API_KEY);
				String locator = System.getenv(WHAT_3_WORDS_API_LOCATOR);

				requireNonNull(key, "what3words API key is missed");
				requireNonNull(locator, "what3words API URL is missed");

				String loggerLevelName = System.getenv(WHAT_3_WORDS_API_LOG_LEVEL);
				Level loggerLevel = (isNull(loggerLevelName)) ? Level.NONE :
						                    Arrays.stream(Level.values())
						                          .filter(level -> loggerLevelName.equalsIgnoreCase(level.name()))
						                          .findAny()
						                          .orElse(Level.NONE);

				threeWordAddressService = Feign.builder()
				                               .client(new Http2Client())
				                               .logger(new Slf4jLogger())
				                               .logLevel(loggerLevel)
				                               .decoder(new What3WordsDecoder())
				                               .errorDecoder(new What3WordsErrorDecoder())
				                               .requestInterceptor(new KeyInterceptor(key))
				                               .target(What3Words.class, locator);
			}
		}

		return threeWordAddressService;
	}

	@VisibleForTesting
	APIGatewayProxyResponseEvent processInlineQuery(JSONObject inlineQuery) {
		LOGGER.trace("Process inline query");

		long inlineQueryId = inlineQuery.getLong(ID);
		ResourceBundle messages = getMessages(getLanguageCode(inlineQuery));

		if (inlineQuery.has(LOCATION)) {
			try {
				SquaredAddress squaredAddress = getSquaredAddress(inlineQuery, messages);

				return getResponseEvent(makeAddressAnswerInlineQuery(inlineQueryId, squaredAddress));
			} catch (FeignException | What3WordsException exception) {
				return getResponseEvent(makeErrorAnswerInlineQuery(inlineQueryId, messages.getString(COULD_NOT_CONVERT_TITLE),
						String.format(messages.getString(COULD_NOT_CONVERT_MESSAGE), exception.getMessage()), exception.getMessage()));
			}
		} else {
			LOGGER.debug("Location or query are not found");
			return getResponseEvent(makeEmptyAnswerInlineQuery(inlineQueryId, messages.getString(EMPTY_RESULT_TITLE),
					messages.getString(EMPTY_RESULT_MESSAGE), messages.getString(EMPTY_RESULT_HINT)));
		}
	}

	@VisibleForTesting
	APIGatewayProxyResponseEvent processMessage(JSONObject message) {
		LOGGER.trace("Process message");

		if (message.has(VIA_BOT)) {
			LOGGER.debug("Ignore message via another bot");
			return responseOK();
		}

		long chatId = message.getJSONObject(CHAT).getLong(ID);
		ResourceBundle messages = getMessages(getLanguageCode(message));

		if (message.has((LOCATION))) {
			try {
				SquaredAddress squaredAddress = getSquaredAddress(message, messages);

				return getResponseEvent(makeAddressMessage(chatId, squaredAddress));
			} catch (FeignException | What3WordsException exception) {
				// do nothing
			}
		} else if (message.has(TEXT) && !message.getString(TEXT).isBlank() && message.has(ENTITIES)) {
			JSONArray entities = message.getJSONArray(ENTITIES);
			String text = message.getString(TEXT);
			boolean isPrivateChat = !GROUP_CHATS.contains(message.getJSONObject(CHAT).getString(TYPE));

			if (HAS_COMMANDS.test(ENTITY_STREAM.apply(entities))) {
				LOGGER.debug("Process command(s)");

				if (START_WITH_COMMAND.test(ENTITY_STREAM.apply(entities))
						    && ONLY_ONE_COMMAND.test(ENTITY_STREAM.apply(entities))) {
					String[] commandTokens = text.split(REGEX_SPACE);
					String command = commandTokens[0].toLowerCase();

					if (PATTERN_COMMAND_HELP.matcher(command).matches()) {
						LOGGER.debug("Help command");

						return getResponseEvent(makeMessage(chatId, messages.getString(HELP_TEXT)));
					} else if (PATTERN_COMMAND_START.matcher(command).matches()) {
						LOGGER.debug("Start command");

						if (isPrivateChat) {
							return getResponseEvent(makeLocationRequest(chatId, messages.getString(WELCOME_TEXT),
									messages.getString(MY_LOCATION_TEXT)));
						}

						return getResponseEvent(makeMessage(chatId, messages.getString(WELCOME_TEXT)));
					}
				}
				// Others commands are ignored
				LOGGER.debug("Commands are ignored: {}", text);
			} else {
				// Not command is not implemented yet
				LOGGER.debug("Not command: {}", text);
			}
		}

		return responseOK();
	}

	private String getLanguageCode(JSONObject request) {
		return request.getJSONObject(FROM).optString(LANGUAGE_CODE, DEFAULT_LANGUAGE);
	}

	private ResourceBundle getMessages(String languageCode) {
		return ResourceBundle.getBundle(MESSAGES_BUNDLE_NAME, Locale.forLanguageTag(languageCode));
	}

	private SquaredAddress getSquaredAddress(JSONObject request, ResourceBundle messages) throws FeignException, What3WordsException {
		String languageCode = getLanguageCode(request);
		JSONObject location = request.getJSONObject(LOCATION);
		BigDecimal latitude = location.getBigDecimal(LATITUDE);
		BigDecimal longitude = location.getBigDecimal(LONGITUDE);

		try {
			WordsRequest.WordsRequestBuilder builder = WordsRequest.builder();

			builder.coordinates(latitude, longitude);
			if (getAvailableLanguages().containsKey(languageCode)) {
				builder.language(getAvailableLanguages().get(languageCode));
			} else if (messages.containsKey(ALTERNATIVE_LANGUAGE)) {
				String alternativeLanguageCode = messages.getString(ALTERNATIVE_LANGUAGE);
				builder.language(Language.builder()
				                         .code(alternativeLanguageCode)
				                         .name(alternativeLanguageCode)
				                         .nativeName(alternativeLanguageCode)
				                         .build());
			}

			SquaredAddress squaredAddress = getWhat3WordsService().convertToAddress(builder.build());

			LOGGER.debug("Get 3wa for {},{}: {}", latitude, longitude, squaredAddress);

			return squaredAddress;
		} catch (FeignException | What3WordsException exception) {
			LOGGER.debug("Coordinates {},{} are not found: {}", latitude, longitude, exception.getMessage(), exception);
			throw exception;
		}
	}

}
