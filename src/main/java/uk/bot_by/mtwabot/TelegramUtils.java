package uk.bot_by.mtwabot;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import uk.bot_by.w3w.SquaredAddress;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static uk.bot_by.mtwabot.TelegramField.CacheTime;
import static uk.bot_by.mtwabot.TelegramField.ChatID;
import static uk.bot_by.mtwabot.TelegramField.Description;
import static uk.bot_by.mtwabot.TelegramField.DisableWebPagePreview;
import static uk.bot_by.mtwabot.TelegramField.Id;
import static uk.bot_by.mtwabot.TelegramField.InlineQueryId;
import static uk.bot_by.mtwabot.TelegramField.InputMessageContent;
import static uk.bot_by.mtwabot.TelegramField.IsPersonal;
import static uk.bot_by.mtwabot.TelegramField.Keyboard;
import static uk.bot_by.mtwabot.TelegramField.MessageText;
import static uk.bot_by.mtwabot.TelegramField.Method;
import static uk.bot_by.mtwabot.TelegramField.OneTimeKeyboard;
import static uk.bot_by.mtwabot.TelegramField.ParseMode;
import static uk.bot_by.mtwabot.TelegramField.ReplyMarkup;
import static uk.bot_by.mtwabot.TelegramField.RequestLocation;
import static uk.bot_by.mtwabot.TelegramField.ResizeKeyboard;
import static uk.bot_by.mtwabot.TelegramField.Results;
import static uk.bot_by.mtwabot.TelegramField.Text;
import static uk.bot_by.mtwabot.TelegramField.ThumbHeight;
import static uk.bot_by.mtwabot.TelegramField.ThumbLocation;
import static uk.bot_by.mtwabot.TelegramField.ThumbWidth;
import static uk.bot_by.mtwabot.TelegramField.Title;
import static uk.bot_by.mtwabot.TelegramField.Type;

public class TelegramUtils {

	private static final String ANSWER_INLINE_QUERY = "answerInlineQuery";
	private static final String CHAT_ICON;
	private static final String EMPTY = "empty";
	private static final String ERROR = "error";
	private static final String ERROR_ICON;
	private static final String HASH = "#";
	private static final String ICON_PATH = "ICON_PATH";
	private static final int ICON_SIZE = 256;
	private static final String INFO_ICON;
	private static final String INLINE_QUERY_RESULT_ARTICLE = "article";
	private static final int NO_CACHE = 0;
	private static final String PARSE_MODE_HTML = "HTML";
	private static final String SEND_MESSAGE = "sendMessage";
	private static final String THREE_SLASHES = "///";
	private static final Boolean WEB_PAGE_PREVIEW_DISABLED = true;

	static {
		String iconPathFormat = System.getenv(ICON_PATH);

		Objects.requireNonNull(iconPathFormat, "Icon path is missed");

		CHAT_ICON = String.format(iconPathFormat, "chat");
		ERROR_ICON = String.format(iconPathFormat, "error");
		INFO_ICON = String.format(iconPathFormat, "info");
	}

	@NotNull
	public static String makeMessage(long chatId, String text) {
		return messageBuilder()
				       .add(Method, SEND_MESSAGE)
				       .add(ChatID, chatId)
				       .add(Text, text)
				       .add(DisableWebPagePreview, WEB_PAGE_PREVIEW_DISABLED)
				       .add(ParseMode, PARSE_MODE_HTML)
				       .build();
	}

	@NotNull
	public static String makeLocationRequest(long chatId, String text, String buttonLabel) {
		List<Map<TelegramField, Object>> firstRow = new ArrayList<>();
		List<List<Map<TelegramField, Object>>> keyboard = new ArrayList<>();
		Map<TelegramField, Object> locationButton = new HashMap<>();
		Map<TelegramField, Object> locationRequest = new HashMap<>();

		locationButton.put(Text, buttonLabel);
		locationButton.put(RequestLocation, true);
		firstRow.add(locationButton);
		keyboard.add(firstRow);
		locationRequest.put(OneTimeKeyboard, true);
		locationRequest.put(Keyboard, keyboard);
		locationRequest.put(ResizeKeyboard, true);

		return messageBuilder()
				       .add(Method, SEND_MESSAGE)
				       .add(ChatID, chatId)
				       .add(Text, text)
				       .add(ReplyMarkup, locationRequest)
				       .add(DisableWebPagePreview, WEB_PAGE_PREVIEW_DISABLED)
				       .build();
	}

	@NotNull
	public static String makeAddressMessage(long chatId, SquaredAddress squaredAddress) {
		return makeMessage(chatId, THREE_SLASHES + squaredAddress.getWords());
	}

	@NotNull
	public static String makeSingleAnswerInlineQuery(long inlineQueryId, @NotNull String title, @NotNull String messageText, String description,
	                                                 String id, String icon, int cacheTime) {
		Map<TelegramField, Object> result = new HashMap<>();

		result.put(Type, INLINE_QUERY_RESULT_ARTICLE);
		result.put(Title, title);
		if (null != description) { // QUESTION: is it still needed?
			result.put(Description, description);
		}
		result.put(Id, id);
		result.put(ThumbLocation, icon);
		result.put(ThumbHeight, ICON_SIZE);
		result.put(ThumbWidth, ICON_SIZE);
		result.put(InputMessageContent, Collections.singletonMap(MessageText, messageText));

		return messageBuilder()
				       .add(Method, ANSWER_INLINE_QUERY)
				       .add(InlineQueryId, inlineQueryId)
				       .add(Results, Collections.singletonList(result))
				       .add(CacheTime, cacheTime)
				       .add(IsPersonal, true)
				       .build();
	}

	@NotNull
	public static String makeAddressAnswerInlineQuery(long inlineQueryId, @NotNull SquaredAddress squaredAddress) {
		return makeSingleAnswerInlineQuery(inlineQueryId, squaredAddress.getWords().toString(), THREE_SLASHES + squaredAddress.getWords(),
				squaredAddress.getNearestPlace(), HASH + squaredAddress.getWords().hashCode(), CHAT_ICON, NO_CACHE);
	}

	@NotNull
	public static String makeEmptyAnswerInlineQuery(long inlineQueryId, @NotNull String title, @NotNull String messageText, String description) {
		return makeSingleAnswerInlineQuery(inlineQueryId, title, messageText, description, EMPTY, INFO_ICON, NO_CACHE);
	}

	@NotNull
	public static String makeErrorAnswerInlineQuery(long inlineQueryId, @NotNull String title, @NotNull String messageText, String description) {
		return makeSingleAnswerInlineQuery(inlineQueryId, title, messageText, description, ERROR, ERROR_ICON, NO_CACHE);
	}

	private static MessageBuilder messageBuilder() {
		return new MessageBuilder();
	}

	private static class MessageBuilder {

		private static final String MESSAGE_FIELD_IS_NULL = "Message field cannot be null";

		private final Map<TelegramField, Object> fieldValues;

		MessageBuilder() {
			fieldValues = new HashMap<>();
		}

		public MessageBuilder add(TelegramField telegramField, Object value) {
			Objects.requireNonNull(telegramField, MESSAGE_FIELD_IS_NULL);

			fieldValues.put(telegramField, value);

			return this;
		}

		public String build() {
			return new JSONObject(fieldValues).toString();
		}

	}

}
