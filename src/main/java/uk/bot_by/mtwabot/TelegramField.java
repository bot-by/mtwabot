package uk.bot_by.mtwabot;

public enum TelegramField {

	CacheTime("cache_time"),
	ChatID("chat_id"),
	Description("description"),
	DisableWebPagePreview("disable_web_page_preview"),
	Id("id"),
	InlineQueryId("inline_query_id"),
	InputMessageContent("input_message_content"),
	IsPersonal("is_personal"),
	Keyboard("keyboard"),
	Latitude("latitude"),
	Longitude("longitude"),
	MessageText("message_text"),
	Method("method"),
	OneTimeKeyboard("one_time_keyboard"),
	ParseMode("parse_mode"),
	ReplyMarkup("reply_markup"),
	ReplyToMessageID("reply_to_message_id"),
	RequestLocation("request_location"),
	ResizeKeyboard("resize_keyboard"),
	Results("results"),
	Text("text"),
	Title("title"),
	ThumbHeight("thumb_height"),
	ThumbLocation("thumb_url"),
	ThumbWidth("thumb_width"),
	Type("type");

	private final String fieldName;

	TelegramField(String fieldName) {
		this.fieldName = fieldName;
	}

	@Override
	public String toString() {
		return fieldName;
	}

}
