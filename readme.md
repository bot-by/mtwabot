# My 3 Word Address Bot

The Telegram bot converts coordinates to [3 word address][3wa].

I have had the similar bot _square3m_, but I couldn't figure out how to use it.
But decided to make a new one inspired by the FitBit application [my 3 words][fitbit-app-my3words]
from the newsletter [what3words][].

## Installation

The bot runs as AWS Lambda.

1.  Create new Java 11 function with basic Lambda permissions.
2.  Create new REST API with permissions to call the function.
3.  Build project then upload the archiv

## Usage

TODO: Write usage instructions

## Contributing

See [Contributing](contributing.md)

## History

See [Changelog](changelog.md)

## Credits

TODO: Write credits

## License

Copyright 2021 Witalij Berdinskich

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[Apache License v2.0](LICENSE)  
[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat)](http://www.apache.org/licenses/LICENSE-2.0.html)

[3wa]: https://what3words.com/how-to "How to use a 3 word address for home and business"
[what3words]: https://what3words.com/ "The simplest way to talk about location"
[fitbit-app-my3words]: https://gallery.fitbit.com/details/059259e1-b9ef-43a9-b25f-4dab7654a734
