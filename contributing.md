# Contributing to My 3 Word Address Bot

My 3 Word Address Bot is an open-source project and all contributions are welcome to assist with its
development and maintenance.

## Translation

[![Crowdin](https://badges.crowdin.net/mtwabot/localized.svg)](https://crowdin.com/project/mtwabot)

This bot is internationalized and available in multiple languages. Translations are managed using the **Crowdin** platform.
After signing up, you can go to the [My 3 Word Address Bot translations page][bot-i18n], select a language and click to start translating.

## Issues (bug and feature tracker)

Please report any bugs found, feature requests or other issues on
[My 3 Word Address Bot GitLab tracker][bot-issues].

When creating a new issue, try following [necolas's guidelines][issue-guidelines].

## Fork, patch and contribute code

Feel free to fork My 3 Word Address Bot's [Git repository at GitLab][bot-gitlab] for your own use and
updates.

Contribute your fixes and new features back to the main codebase using
[GitLab merge requests][gitlab-merge-requests].

[bot-i18n]: https://crowdin.com/project/mtwabot
[bot-issues]: https://gitlab.com/bot-by/mtwabot/-/issues
[issue-guidelines]: http://github.com/necolas/issue-guidelines/#readme
[bot-gitlab]: https://gitlab.com/bot-by/mtwabot/
[gitlab-merge-requests]: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html
